/**
 * AngularJS
 * @author Wahyu Sanjaya <wahyu.sanjaya@emeriocorp.com>
 */

var mainApp = angular.module('mainApp', [
  'ngRoute'
]);

mainApp.config(function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "templates/page1.html", controller: ""})
    // Pages
    .when("/page2", {templateUrl: "templates/page2.html", controller: ""})
    // else 404
    .otherwise("/404", {templateUrl: "templates/404.html", controller: ""});
});
